var Background = cc.LayerColor.extend({
	ctor: function(){
		this._super();
		var director = cc.Director.getInstance();
		var winSize = director.getWinSize();
		var centerPos = cc.p(winSize.width/2, winSize.height/2);

		var bg = cc.Sprite.create('images/background2.jpg');
		bg.setPosition(centerPos);
		this.addChild(bg);

		this.setPosition(0,0);

		// this.followBall = cc.Follow.create( this.newJumper, cc.rect( 0, 0, 1600, 1040) );
  //       this.runAction(this.followBall);

        this.scheduleUpdate();
        return true;

	}
	


});

